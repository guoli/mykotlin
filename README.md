参考网址：
https://www.runoob.com/kotlin/kotlin-tutorial.html

https://juejin.im/post/591dd9f544d904006c9fbb96

本人项目连接：
https://gitlab.com/guoli/mykotlin

注意Kotlin：不再需要;分号，换行就好。

一、定义变量常量、函数、区间、字符串模板

１.定义变量常量 
　可变：var <标识符> : <类型> = <初始化值>　
　不可变：val <标识符> : <类型> = <初始化值>

2.函数 定义使用关键字 fun，参数格式为：参数 : 类型 
　例子：fun study() |例子： 有返回值的fun study(): Int

3.区间:表达式由具有操作符形式 .. 的 rangeTo 函数辅以 in 和 !in 形成。
　for (i in 1..4) print(i) // 输出“1234”

4.字符串模板:
```
val s2 = "${s1.replace("is", "was")}, but now is $a"
```



二、when 替代switch 换行，替代case else 替代default
```
 var x = 0
when (x) {
1 -> println("x == 1")
 2 -> println("x == 2")
else -> { // 注意这个块
println("x 不是 1 ，也不是 2")
    }
 }
```

三、类定义、继承、重写、接口实现、构造函数
 1.继承 open:如果一个类要被继承，可以使用 open 关键字进行修饰。
2.重写 override:子类重写方法使用 override 关键词
3.接口实现 implemented extends 都使用：　
4.主构造函数  Person(var name : String, var age : Int)
```
        open class Person(var name : String, var age : Int){// 基类

        open fun study(){       // 允许子类重写
            println("我毕业了")
        }
             fun clazz(): Int{       //
                println("我毕业了")
                return 0
            }
        }

        class Student(name : String, age : Int, var no : String, var score : Int) : Person(name, age) {
            override fun study(){
                for (i in 1..4) print(i) // 输出“1234”
                println("我在读大学")
            }
        }
```