package com.lg.mykotlin

import android.app.Activity
import android.os.Bundle

/**
 * create by liguo on 19-5-13
 */
class TestActivity : Activity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
    }
}
