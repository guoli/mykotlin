package com.lg.mykotlin

/**
 *create by liguo on 19-5-13
 */
class learnKotlin {
    companion object {
        var a = 1
        // 模板中的简单名称：
        val s1 = "a is $a"

        // 模板中的任意表达式：
        val s2 = "${s1.replace("is", "was")}, but now is $a"

        /*
        * 1.字段后加!!像Java一样抛出空异常.
        * 2.另一种字段后加?可不做处理返回值为 null或配合?:做空判断处理
        * */
        //类型后面加?表示可为空
        var age: String? = "23"
        //抛出空指针异常
        val ages = age!!.toInt()
        //不做处理返回 null
        val ages1 = age?.toInt()
        //age为空返回-1
        val ages2 = age?.toInt() ?: -1

        /*
        * is instanceof
        *
        *
        * */fun getStringLength(obj: Any): Int? {
            if (obj is String) {
                // 做过类型判断以后，obj会被系统自动转换为String类型
                return obj.length
            }

            //在这里还有一种方法，与Java中instanceof不同，使用!is
            // if (obj !is String){
            //   // XXX
            // }

            // 这里的obj仍然是Any类型的引用
            return null
        }
        //注意Kotlin：不再需要;分号，换行就好。

        //一、类定义、继承、重写、接口实现、构造函数
        //1.继承 open:如果一个类要被继承，可以使用 open 关键字进行修饰。
        //2.重写 override:子类重写方法使用 override 关键词
        //3.接口实现 implemented extends 都使用：　
        //4.主构造函数  Person(var name : String, var age : Int)

        //二、定义变量常量、函数、区间、字符串模板
        //１.定义变量常量 可变：var <标识符> : <类型> = <初始化值>　不可变：val <标识符> : <类型> = <初始化值>
        //2.函数 定义使用关键字 fun，参数格式为：参数 : 类型 例子：fun study() |例子： 有返回值的fun study(): Int
        //3.区间:表达式由具有操作符形式 .. 的 rangeTo 函数辅以 in 和 !in 形成。for (i in 1..4) print(i) // 输出“1234”
        //4.字符串模板:val s2 = "${s1.replace("is", "was")}, but now is $a"
        //  $表示一个变量名或者变量值
        //  $varName 表示变量值
        //  ${varName.fun()} 表示变量的方法返回值:
        open class Person(var name : String, var age : Int){// 基类

        open fun study(){       // 允许子类重写
            println("我毕业了")
        }
             fun clazz(): Int{       //
                println("我毕业了")
                return 0
            }
        }

        class Student(name : String, age : Int, var no : String, var score : Int) : Person(name, age) {
            override fun study(){
                for (i in 1..4) print(i) // 输出“1234”
                println("我在读大学")

            }
        }

        //接口
        interface A {
            fun foo() { print("A") }   // 已实现
            fun bar()                  // 未实现，没有方法体，是抽象的
        }

        interface B {
            fun foo() { print("B") }   // 已实现
            fun bar() { print("bar") } // 已实现
        }

        class C : A {
            override fun bar() { print("bar") }   // 重写
        }

        class D : A, B {
            override fun foo() {
                super<A>.foo()
                super<B>.foo()
            }

            override fun bar() {
                super<B>.bar()
            }
        }


        // 创建接口
        interface Base {
            fun print()
        }

        // 实现此接口的被委托的类
        class BaseImpl(val x: Int) : Base {
            override fun print() { print(x) }
        }

        // 通过关键字 by 建立委托类
        class Derived(b: Base) : Base by b

        @JvmStatic fun main(args: Array<String>) {
            //println(s2)
            //区间　..
            print("循环输出：")
            for (i in 1..4) print(i) // 输出“1234”
            println("\n----------------")
            print("设置步长：")
            for (i in 1..4 step 2) print(i) // 输出“13”
            println("\n----------------")
            print("使用 downTo：")
            for (i in 4 downTo 1 step 2) print(i) // 输出“42”
            println("\n----------------")
            print("使用 until：")
            // 使用 until 函数排除结束元素
            for (i in 1 until 4) {   // i in [1, 4) 排除了 4
                print(i)
            }
            println("\n----------------")

            //数组[1,2,3]
            val a = arrayOf(1, 2, 3)
            println("a[2]="+a[2])

            //when 替代　switch 换行　替代case else 替代default
            var x = 0
            when (x) {
                1 -> println("x == 1")
                2 -> println("x == 2")
                else -> { // 注意这个块
                    println("x 不是 1 ，也不是 2")
                }
            }

            when (x) {
                in 0..10 -> println("x 在该区间范围内")
                else -> println("x 不在该区间范围内")
            }

            val items = setOf("apple", "banana", "kiwi")
            when {
                "orange" in items -> println("juicy")
                "apple" in items -> println("apple is fine too")
            }

            //for循环
            val items1 = listOf("apple", "banana", "kiwi")
            for (item in items1) {
                println(item)
            }

            for (index in items1.indices) {
                println("item at $index is ${items1[index]}")
            }

            //类
            var person = Person()
            person.lastName = "wang"

            println("lastName:${person.lastName}")

            person.no = 9
            println("no:${person.no}")

            person.no = 20
            println("no:${person.no}")


            //类的构造器
            var runoob =  Runoob("菜鸟教程", 10000)
            println(runoob.siteName)
            println(runoob.url)
            println(runoob.country)
            runoob.printTest()


            val s =  Student("Runoob", 18, "S12346", 89)
            println("学生名： ${s.name}")
            println("年龄： ${s.age}")
            println("学生号： ${s.no}")
            println("成绩： ${s.score}")
            s.study()

            val d =  D()
            d.foo();
            d.bar();

            val b = BaseImpl(10)
            Derived(b).print() // 输出 10
        }
    }
}