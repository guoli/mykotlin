package com.lg.mykotlin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * create by liguo on 19-5-13
 */
public class JavaMainActivity extends Activity implements View.OnClickListener {
    TextView tv_hello;
    Button btn_intent;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context =this;
        initView();
    }

    private void initView() {
        tv_hello=findViewById(R.id.tv_hellworld);
        tv_hello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"hello world",Toast.LENGTH_SHORT).show();
            }
        });
        btn_intent =findViewById(R.id.btn_intent);
        btn_intent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(context,TestActivity.class);
                Bundle bundle = new Bundle();
                it.putExtras(bundle);
                startActivity(it);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {

    }
}
