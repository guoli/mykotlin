package com.lg.mykotlin

sealed class UiOp {
        object Show: UiOp()
        object Hide: UiOp()
        class TranslateX(val px: Float): UiOp()
        class TranslateY(val px: Float): UiOp()
    }