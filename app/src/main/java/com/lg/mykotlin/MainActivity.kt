package com.lg.mykotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() ,View.OnClickListener{
    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val instance by lazy { this } //这里使用了委托，表示只有使用到instance才会执行该段代码
    val c: Int = 1//不可变变量定义
    var d: String=""//可变变量定义 var <标识符> : <类型> = <初始化值>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main);
        initView();

    }

    private fun initView() {
        btn_intent.setOnClickListener(){
            var a=  vars(1,3,5)
            Toast.makeText(this,"sum="+a,Toast.LENGTH_SHORT).show();
        }
        tv_hellworld.setOnClickListener(){
            val it = Intent(instance, TestActivity::class.java)
            val bundle = Bundle();
            it.putExtras(bundle);
            startActivity(it);
        }

    }





    fun execute(view: View, op: UiOp) = when (op) {
        UiOp.Show -> view.visibility = View.VISIBLE
        UiOp.Hide -> view.visibility = View.GONE
        is UiOp.TranslateX -> view.translationX = op.px // 这个 when 语句分支不仅告诉 view 要水平移动，还告诉 view 需要移动多少距离，这是枚举等 Java 传统思想不容易实现的
        is UiOp.TranslateY -> view.translationY = op.px
    }

    /*
    * 返回值函数
    * */
    fun sum(a: Int, b: Int): Int {   // Int 参数，返回值 Int
        return a + b
    }


    /*
    * 可变长参数函数
    *
    * */
    fun vars(vararg v:Int):Int{
        var a=0
        for(vt in v){
            a=sum(a,vt)
        }
        return a;
    }


}


